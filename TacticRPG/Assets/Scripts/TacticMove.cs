﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticMove : MonoBehaviour
{
    List<Tiles> selectableTiles = new List<Tiles>();
    GameObject[] tiles;

    Stack<Tiles> path = new Stack<Tiles>();
    Tiles CurrentTile;

    public bool moving = false;
    public int move = 5;
    public float jumphight = 2;
    public float moveSpeed = 2;

    Vector3 velocity = new Vector3();
    Vector3 heading = new Vector3();

    float halfheight = 0;

    public void Init()
    {
        tiles = GameObject.FindGameObjectsWithTag("Tile");

        halfheight = GetComponent<Collider>().bounds.extents.y;
    }

    public void GetCurrentTile()
    {
        CurrentTile = GetTargetTile(gameObject);
        CurrentTile.current = true;
    }

    public Tiles GetTargetTile(GameObject target)
    {
        RaycastHit hit;
        Tiles tile = null;

        if (Physics.Raycast(target.transform.position, -Vector3.up, out hit, 1))
        {
            tile = hit.collider.GetComponent<Tiles>();
        }
        return tile;
    }

    public void ComputeAdjacencyLists()
    {
        foreach (GameObject tile in tiles)
        {
            Tiles t = tile.GetComponent<Tiles>();
            t.FindNeighbors(jumphight);
        }
    }

    public void FindSelectableTiles()
    {
        ComputeAdjacencyLists();
        GetCurrentTile();
        Queue<Tiles> process = new Queue<Tiles>();

        process.Enqueue(CurrentTile);
        CurrentTile.visited = true;
        while(process.Count > 0)
        {
            Tiles t = process.Dequeue();

            selectableTiles.Add(t);
            t.selectable = true;

            if (t.distance < move)
            {
                foreach (Tiles tile in t.neighbors)
                {
                    if (!tile.visited)
                    {
                        tile.parent = t;
                        tile.visited = true;
                        tile.distance = 1 + t.distance;
                        process.Enqueue(tile);
                    }
                }
            }
        }
    }

    public void MoveToTile(Tiles tile)
    {
        path.Clear();
        tile.target = true;
        moving = true;

        Tiles next = tile;
        while (next != null)
        {
            path.Push(next);
            next = next.parent;
        }

    }

    void CalculateHeading(Vector3 target)
    {
        heading = target - transform.position;
        heading.Normalize();

    }

    void SetHorizontalVelocity()
    {
        velocity = heading * moveSpeed;
    }

    public void Move()
    {
        if(path.Count > 0)
        {
            Tiles t = path.Peek();
            Vector3 target = t.transform.position;

            target.y += halfheight + t.GetComponent<Collider>().bounds.extents.y;

            if(Vector3.Distance(transform.position,target) >= 0.05f)
            {
                CalculateHeading(target);
                SetHorizontalVelocity();

                transform.forward = heading;
                transform.position += velocity * Time.deltaTime;
            }
            else
            {
                transform.position = target;
                path.Pop();
            }

        }
        else
        {
            RemoveSelectableTiles();
            moving = false;
        }
    }

    private void RemoveSelectableTiles()
    {
        if (CurrentTile != null)
        {
            CurrentTile.current = false;
            CurrentTile = null;
        }
        foreach(Tiles tile in selectableTiles)
        {
            tile.Reset();
        }
        selectableTiles.Clear();
    }
}
